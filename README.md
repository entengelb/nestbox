Nestbox
=======

Recreate base box
----

Run this script:

    ./repack.sh


Initialize new vagrant box
----

Cange the ip adress of the target box in the Vagrantfile.dist to avoid conflicts with more than one running box:

    config.vm.network "private_network", ip: "31.37.31.31"

Also change the hostname of the box:

    config.vm.host_name = "nestbox.dev"

nestbox is preconfigured as a PHP-development box. You can change the "role" at the custom facter facts section in the Vagrantfile:

    puppet.facter = {
        "ssh_username" => "vagrant",
        "role"        => "php-server",
        "environment" => "dev"
    }

Add your custom hiera configuration at `provision/puppet/hieradata`.  
Here a copy of the hiera.yaml:

    ---
    :backends: yaml
    :yaml:
        :datadir: '/vagrant/provision/puppet/hieradata'
    :hierarchy:
        - "%{::hostname}"
        - "%{::environment}"
        - "%{::role}"
        - common
