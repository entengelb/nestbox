#!/bin/bash
PUPPETDIR=${1:-`pwd`/../puppet} #TODO: Check, if this works
# Run puppet
sudo puppet apply ${PUPPETDIR}/manifests/site.pp --modulepath=${PUPPETDIR}/modules --hiera_config ${PUPPETDIR}/hiera.yaml --parser future
