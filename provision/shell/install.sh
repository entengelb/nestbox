#!/bin/bash

# update apt

# install puppet 3
if ! type puppet >/dev/null; then
    echo "--> start install puppet <--"
    sudo wget --no-check-certificate http://apt.puppetlabs.com/puppetlabs-release-trusty.deb
    sudo dpkg -i puppetlabs-release-trusty.deb
    sudo apt-get -y update
    sudo apt-get -y install puppet-common
    echo "<-- puppet was installed -->"
fi

# TODO: checko for ruby version
sudo apt-get install ruby1.9.1-dev

# install rubygems
if ! type gem >/dev/null; then
    echo "--> start install rubygems <--"
    sudo apt-get -y install rubygems
    echo "<-- rubygems installed -->"
fi

# install librarian-puppet
#if ! gem list librarian-puppet -i >/dev/null; then
#    echo "--> start install librarian-puppet <--"
#    sudo gem install librarian-puppet
#    echo "<-- librarian-puppet installed -->"
#fi

if ! type git >/dev/null; then
    echo "--> start install git <--"
    sudo apt-get -y install git
    echo "<-- git installed -->"
fi

if ! type augtool >/dev/null; then
    echo "--> start install augeas <--"
    sudo apt-get -y install augeas-tools
    echo "<-- augeas installed -->"
fi
