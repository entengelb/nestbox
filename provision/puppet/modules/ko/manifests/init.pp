# == Class: ko
#
#   main klass for nestbox
#   perhaps, rename this to nestbox
#
#
# === Parameters
#
# [*home_user*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# [*home_user*]
#   The main user of the nestbox. Should be 'vagrant',
#   when using with Vagrant
#
# [*default_packages*]
#   The default packages, that should be installed.
#
#
# === Examples
#
#  class { 'ko':
#  home_user => 'vagrant',
#  default_packages = ['curl', 'acl']
#  }
#
# === Authors
#
# Lukas Sadzik <entengelb@gmail.com>
#
#
class ko (
  $home_user = 'vagrant',
  $default_packages = ['curl', 'acl']
){
  package { $default_packages:
    ensure => installed,
  }

  user { $home_user:
    ensure  => present,
    comment => 'home user',
    home    => "/home/${home_user}",
    shell   => '/bin/bash',
  }

  file { "/home/${home_user}":
    ensure  => directory,
    owner   => $home_user,
    group   => $home_user,
    require => User[$home_user],
  }

  file { "/home/${home_user}/projects/":
    ensure => directory,
    owner  => $home_user,
    group  => $home_user,
  }
}