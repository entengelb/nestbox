# Define: ko::project
# Parameters:
# arguments
define ko::project (
  $type         = 'default',
  $source       = '',
  $docroot      = "/var/www/${name}",
  $project_home = "/home/vagrant/projects/${name}",
) {

  if 'git' == $type {
    exec { "project_checkout_${name}":
      command => "/usr/bin/git clone ${source} ${project_home}",
      creates => $project_home,
    }
  }

  file {"/var/www/${name}":
    ensure  => link,
    target  => $project_home,
    notify  => Service['apache2'],
    owner   => 'vagrant',
    group   => 'vagrant',
  }

  ko::apache::vhost { $name :
    docroot => $docroot,
  }
}