class ko::php (
  $modules = ['php5-intl', 'php5-xdebug']
){
  package { ['php5', 'php5-cli']:
    ensure => installed,
  }

  package { $modules:
    ensure => installed,
  }
}