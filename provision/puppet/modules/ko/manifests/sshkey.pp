define ko::sshkey(
  $private_key_file,
  $public_key_file,
  $owner = 'vagrant',
  $group = 'vagrant',
) {
  file { 'ko_ssh_dir':
    ensure => directory,
    path   => "/home/${owner}/.ssh",
    owner  => $owner,
    group  => $group,
    mode   => '0400',
  }

  file { 'ko_ssh_private_file':
    ensure  => file,
    path    => "/home/${owner}/.ssh/id_rsa",
    content => file($private_key_file),
    owner   => $owner,
    group   => $group,
    mode    => '0400',
  }

  file { 'ko_ssh_public_file':
    ensure  => file,
    path    => "/home/${owner}/.ssh/id_rsa.pub",
    content => file($public_key_file),
    owner   => $owner,
    group   => $group,
    mode    => '0400',
  }

  file { 'ko_ssh_config':
    ensure => file,
    path   => "/home/${owner}/.ssh/config",
    source => 'puppet:///modules/ko/ssh/config',
    owner  => $owner,
    group  => $group,
    mode   => '0400',
  }
}