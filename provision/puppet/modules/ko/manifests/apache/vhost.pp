define ko::apache::vhost (
  $port     = 80,
  $docroot  = "/var/www/${name}",
  $template = 'simple',
  $enabled  = true,
) {
  file { "vhost_file_${name}":
    ensure  => file,
    path    => "/etc/apache2/sites-available/${name}.conf",
    content => template("ko/apache/vhost_${template}.erb"),
    group   => 'root',
    owner   => 'root',
      }

  if true == $enabled {
    $enabledFile = link
  } else {
    $enabledFile = absent
  }

  file { "/etc/apache2/sites-enabled/${name}.conf":
    ensure  => $enabledFile,
    target  => "/etc/apache2/sites-available/${name}.conf",
    require => File["vhost_file_${name}"],
    notify  => Service['apache2'],
  }
}