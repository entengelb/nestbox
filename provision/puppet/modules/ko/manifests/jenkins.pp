class ko::jenkins {
  package { 'openjdk-7-jre-headless':
    ensure => installed,
  }->
  package { 'jenkins':
    ensure  => installed,
  }
}
