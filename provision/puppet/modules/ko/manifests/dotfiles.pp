class ko::dotfiles (
  $repo,
  $install_dir    = '/home/vagrant/dotfiles',
  $install_script = 'bootstrap.sh -f',
) {
  exec { "clone_dotfiles_repo":
      command => "/usr/bin/git clone ${repo} ${install_dir}",
      creates => $install_dir,
  }->
  exec { "run_dofiles_installation":
      command => "${install_dir}/${install_script}",
  }
}