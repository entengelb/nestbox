class ko::composer {
  exec { 'install_composer':
    command => 'curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    user    => 'root',
    creates => '/bin/composer',
    require => Package['php5', 'curl'],
  }->
  exec {'rename_to_executable':
    command => 'mv /bin/composer.phar /bin/composer',
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    user    => 'root',
    creates => '/bin/composer',
  }
}
