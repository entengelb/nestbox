class ko::apache {
    package { 'apache2':
        ensure  => installed,
    }->
    package { 'libapache2-mod-php5':
        ensure  => installed,
        notify  => Service['apache2'],
    }->
    exec { 'enable_rewrite_mode':
        command => '/usr/sbin/a2enmod rewrite',
        require => Package['apache2'],
    }->
    file {[
            '/etc/apache2/sites-available/default',
            '/etc/apache2/sites-available/000-default.conf',
            '/etc/apache2/sites-enabled/000-default',
            '/etc/apache2/sites-enabled/000-default.conf',
        ]:
        ensure  => absent,
        require => Package['apache2'],
    }->
    service { 'apache2':
        ensure  => running,
        enable  => true,
        require => Package['apache2'],
    }

}
