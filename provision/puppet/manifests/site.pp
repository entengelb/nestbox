node 'default' {
    if hiera('classes', undef) {
        hiera_include('classes')
    }

    $project_configuration = hiera('projects', {})
    create_resources(ko::project, $project_configuration)        
}
