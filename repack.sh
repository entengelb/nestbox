#!/bin/bash

rm vagrant_nestbox.box
packer build --only=virtualbox-iso packer.json
vagrant box remove nestbox
vagrant box add nestbox vagrant_nestbox.box
